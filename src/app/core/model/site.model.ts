export interface Site {
    id: string;
    name: string;
    trafficOut: [number, number][];
    unit: string;
}
