import { Component, OnInit } from "@angular/core";

@Component({
    selector: "la-not-found",
    template: `
        <div class="container">
            <section class="flow column">
                <img src="assets/lost.jpg" />
            </section>
        </div>
    `,
    styles: [
        `
            img {
                width: 400px;
                margin: auto;
            }
        `,
    ],
})
export class NotFoundComponent implements OnInit {
    constructor() {}

    ngOnInit() {}
}
