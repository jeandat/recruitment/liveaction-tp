import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    OnDestroy,
    OnInit,
} from "@angular/core";
import { FormControl } from "@angular/forms";
import { select, Store } from "@ngrx/store";
import { Subscription } from "rxjs";
import { filter, takeUntil } from "rxjs/operators";
import { Aggregation } from "src/app/site/site-traffic/site-traffic.component";
import { TOV_GetSite, TOV_GetSiteList } from "../../site/store/site.actions";
import { siteSelectors } from "../../site/store/site.selectors";
import { BaseComponent } from "../base.component";
import { Site } from "../model/site.model";
import { AppError } from "../network/app-error";
import { SnackBarService } from "../snackbar/snackbar.service";
import { AppState } from "../store/core.reducer";

/*
    I assumed here that fetching sites only return high level info.
    If you want complete data with trafficOut property you have to fetch a single site.
    I'm assuming that's what you want regarding the fact there is an update button in view.
    Depending on volumetry and data model, other scenarios would have been imaginable without confirmation buttons
    nor subsequent fetch for instance.
*/

@Component({
    selector: "la-home",
    templateUrl: "./home.component.html",
    styleUrls: ["./home.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeComponent extends BaseComponent implements OnInit, OnDestroy {
    siteControl: FormControl = new FormControl();
    sites: Site[];
    currentSite: Site;

    private avg: Aggregation;
    private max: Aggregation;

    private currentSiteSub: Subscription;

    constructor(
        private snackbar: SnackBarService,
        private store: Store<AppState>,
        private cdr: ChangeDetectorRef
    ) {
        super();
        this.avg = {
            label: "Average",
            fn: (site: Site) => {
                const sum = site.trafficOut.reduce((acc, point) => {
                    acc += point[1];
                    return acc;
                }, 0);
                return sum / site.trafficOut.length;
            },
        };
        this.max = {
            label: "Max",
            fn: (site: Site) => {
                return Math.max(...site.trafficOut.map((point) => point[1]));
            },
        };
    }

    ngOnInit() {
        this.listenToErrors();
        this.listenToSiteList();
    }

    ngOnDestroy(): void {
        if (this.currentSiteSub) this.currentSiteSub.unsubscribe();
    }

    listenToErrors() {
        this.store
            .pipe(
                select(siteSelectors.selectError),
                filter((err: AppError) => err != null && !err.processed),
                takeUntil(this.done)
            )
            .subscribe((err) => {
                this.snackbar.showError("Server is temporarily unavailable");
            });
    }

    // Refresh current selected site when site change in store
    listenToCurrentSite() {
        if (this.currentSiteSub) this.currentSiteSub.unsubscribe();
        this.currentSiteSub = this.store
            .pipe(
                select(siteSelectors.selectCurrentSite),
                filter((newSite: Site) => {
                    if (newSite == null) return false;
                    if (this.currentSite == null) return newSite != null;
                    return newSite.id !== this.currentSite.id;
                }),
                takeUntil(this.done)
            )
            .subscribe((newSite: Site) => {
                this.currentSite = newSite;
                this.siteControl.setValue(this.findSite(newSite.id));
            });
    }

    // Refresh select when site list is updated in store
    listenToSiteList() {
        this.store
            .pipe(
                select(siteSelectors.selectAll),
                filter((sites: Site[]) => sites != null && sites.length > 0),
                takeUntil(this.done)
            )
            .subscribe((sites: Site[]) => {
                this.sites = sites;
                if (sites && sites.length) this.changeCurrentSite(sites[0].id);
                this.listenToCurrentSite();
                this.cdr.markForCheck();
            });
        this.store.dispatch(new TOV_GetSiteList());
    }

    siteIdentity(_index, site: Site) {
        return site.id;
    }

    cancel() {
        this.siteControl.setValue(this.findSite(this.currentSite.id));
    }

    changeCurrentSite(siteId?: Site["id"]) {
        this.store.dispatch(
            new TOV_GetSite({ id: siteId || this.siteControl.value.id })
        );
    }

    findSite(id: string): Site {
        if (!id || !this.sites) return;
        return this.sites.find((site) => site.id === id);
    }
}
