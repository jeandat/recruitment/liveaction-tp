import { Pipe, PipeTransform } from "@angular/core";
import { memoize } from "src/app/utils";

@Pipe({ name: "humanFormat" })
export class HumanFormatPipe implements PipeTransform {
    constructor() {}

    @memoize()
    transform(value: number, options?: { unit?: string }) {
        console.log("human format pipe invoked with:", value);
        return humanFormat(value, {
            decimals: 2,
            unit: options ? options.unit : "",
        });
    }
}
