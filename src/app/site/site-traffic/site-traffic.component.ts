import {
    ChangeDetectionStrategy,
    Component,
    Input,
    OnChanges,
    SimpleChanges,
} from "@angular/core";
import * as Highcharts from "highcharts";
import { HumanFormatPipe } from "src/app/site/human-format.pipe";
import { Site } from "../../core/model/site.model";

export interface Aggregation {
    fn: (site: Site) => number;
    label: string;
}

@Component({
    selector: "la-site-traffic",
    templateUrl: "./site-traffic.component.html",
    styleUrls: ["./site-traffic.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SiteTrafficComponent implements OnChanges {
    @Input() site: Site;
    @Input() title: string;
    @Input() color: string;
    @Input() aggregation: Aggregation;

    // Highcharts
    // ----------

    Highcharts = Highcharts; // required
    chartOptions: any;

    constructor(private humanFormatPipe: HumanFormatPipe) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (!changes.site.currentValue) return;
        const self = this;
        this.chartOptions = {
            chart: {
                type: "line",
            },
            title: {
                text: this.title,
                align: "left",
                margin: 25,
            },
            xAxis: {
                type: "datetime",
            },
            yAxis: {
                title: "",
                tickInterval: 20 * 1000 * 1000,
                labels: {
                    formatter: (context) =>
                        context.value
                            ? self.humanFormatPipe.transform(context.value, {
                                  unit: self.site.unit,
                              })
                            : 0,
                },
            },
            tooltip: {
                pointFormatter: function () {
                    return (
                        this.series.name +
                        ": " +
                        self.humanFormatPipe.transform(this.y, {
                            unit: self.site.unit,
                        })
                    );
                },
            },
            series: [
                {
                    name: "(AVC) Traffic Out",
                    color: this.color,
                    data: this.site.trafficOut.map(([time, size]) => [
                        time * 1000,
                        size,
                    ]),
                },
            ],
            plotOptions: {
                series: {
                    marker: {
                        enabled: false,
                    },
                },
            },
        };
    }
}
