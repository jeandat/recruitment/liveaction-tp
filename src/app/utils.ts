import * as memoizee from "memoizee";

const defaultConfig: memoizee.Options<any> = { length: false };

/**
Typescript annotation that allows to cache results from the decorated method.
See https://github.com/medikoo/memoizee for config options.
*/
export function memoize(config?: memoizee.Options<any>) {
    return function (
        _target: any,
        _key: string,
        descriptor: PropertyDescriptor
    ) {
        const decoratedFunction = descriptor.value;
        const newFunction = memoizee(decoratedFunction, {
            ...defaultConfig,
            ...config,
        });
        descriptor.value = function () {
            return newFunction.apply(this, arguments);
        };
    };
}
